<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Github
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToGithub()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Github
     * @return void
     */
    public function handleGithubCallback()
    {
        $user = Socialite::driver('github')->user();

        $this->userLoginOrRegister($user);

        return redirect()->route('home');
    }

    /**
     * Google
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Google
     * @return void
     */
    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->user();

        $this->userLoginOrRegister($user);

        return redirect()->route('home');
    }

    /**
     * Не знал какой пароль сохранять пользователю,
     * поставил чтобы был такой же как и емейл
     *
     * @param $data
     * @return void
     */
    protected function userLoginOrRegister($data)
    {
        $userModel = User::where('email', '=', $data->email)->first();

        if (!$userModel) {
            $userModel = new User();
            $userModel->name = $data->name;
            $userModel->email = $data->email;
            $userModel->password = Hash::make($data->email);
            $userModel->provider_id = $data->id;
            $userModel->save();
        }

        Auth::login($userModel);
    }
}

