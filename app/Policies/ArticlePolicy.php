<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Article $article)
    {
        if($user->id == $article->user_id) {
            return true;
        }

        if (
            $user->id !== $article->user_id
            and (
                $user->role == User::ROLE_ADMIN
                or $user->role == User::ROLE_MODERATOR
            )
        ) {
            return true;
        }
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        return $user->role == User::ROLE_ADMIN
            or $user->role == User::ROLE_CREATOR;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Article $article)
    {
        if (
            $user->role == User::ROLE_ADMIN
            or $user->role == User::ROLE_MODERATOR
        ) {
            return true;
        } elseif ($user->role == User::ROLE_CREATOR) {
            return $user->id == $article->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Article $article)
    {
        if (
            $user->role == User::ROLE_ADMIN
            or $user->role == User::ROLE_CREATOR
        ) {
            return true;
        }

        return false;
    }
}
