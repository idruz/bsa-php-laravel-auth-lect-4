<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function ($router) {
    Route::resource('article', ArticleController::class, ['except' => ['show']]);
});

Route::get('/article/{article}', [ArticleController::class, 'show'])->name('article.show');


Route::prefix('auth')->group(function (){
    Route::get('/github', [LoginController::class, 'redirectToGithub'])->name('auth.github');
    Route::get('/github/callback', [LoginController::class, 'handleGithubCallback']);

    Route::get('/google', [LoginController::class, 'redirectToGoogle'])->name('auth.google');
    Route::get('/google/callback', [LoginController::class, 'handleGoogleCallback']);
});
