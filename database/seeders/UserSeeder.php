<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'admin',
                'email' => 'admin@admin.admin',
                'password' => Hash::make('admin@admin.admin'),
                'role' => User::ROLE_ADMIN
            ],
            [
                'name' => 'creator',
                'email' => 'creator@creator.creator',
                'password' => Hash::make('creator@creator.creator'),
                'role' => User::ROLE_CREATOR
            ],
            [
                'name' => 'moderator',
                'email' => 'moderator@moderator.moderator',
                'password' => Hash::make('moderator@moderator.moderator'),
                'role' => User::ROLE_MODERATOR
            ],
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
